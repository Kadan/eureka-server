package com.ilabservice.video.eurekaServer;


import com.amazonaws.encryptionsdk.AwsCrypto;
import com.amazonaws.encryptionsdk.CryptoResult;
import com.amazonaws.encryptionsdk.kms.KmsMasterKey;
import com.amazonaws.encryptionsdk.kms.KmsMasterKeyProvider;

import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.Map;

public class KMSTester {

//    private static final byte[] EXAMPLE_DATA = "Hello World".getBytes(StandardCharsets.UTF_8);

    public static void main(final String[] args) {

        final  String keyArn = "arn:aws:kms:ap-southeast-1:460811381319:key/edd8576d-f2a9-477e-9d2d-b5a31233270e";

        final  String data = "this is my sensitive data";

        // Instantiate the SDK
        final AwsCrypto crypto = new AwsCrypto();

        // Set up the master key provider
        final KmsMasterKeyProvider prov = new KmsMasterKeyProvider(keyArn);

        // Encrypt the data
        //
        // NOTE: Encrypted data should have associated encryption context
        // to protect integrity. For this example, just use a placeholder
        // value. For more information about encryption context, see
        // https://amzn.to/1nSbe9X (blogs.aws.amazon.com)
        final Map<String, String> context = Collections.singletonMap("Example", "String");

        final CryptoResult<byte[], KmsMasterKey> encryptResult = crypto.encryptData(prov, data.getBytes(), context);
//        final String  ciphertext = crypto.encryptString(prov, data, context).getResult();
        final byte[] ciphertext = encryptResult.getResult();


        String encoded = Base64.getEncoder().encodeToString(ciphertext);
        System.out.println("encypted ======》 : " + encoded);




        String myresult = "AYADeJZS0nve8huUVRZOEJE3p+0AcAACAAdFeGFtcGxlAAZTdHJpbmcAFWF3cy1jcnlwdG8tcHVibGljLWtleQBEQWkzYm1oRXFMY3NZM1JuMWNhZzVROGlNeUZJUkY4NlBpa3krdTRWUlpXKzNjVjgrUHRuT1hyc2RtN0JDaTVWa3V3PT0AAQAHYXdzLWttcwBQYXJuOmF3czprbXM6YXAtc291dGhlYXN0LTE6NDYwODExMzgxMzE5OmtleS9lZGQ4NTc2ZC1mMmE5LTQ3N2UtOWQyZC1iNWEzMTIzMzI3MGUAuAECAQB4usFeKqrQTrGO6ZRidytUK3wvVdaed+GjQ/2XN95hrBABL1Wj+6ZB2dbQ0CNHGd7c+QAAAH4wfAYJKoZIhvcNAQcGoG8wbQIBADBoBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDFQcPS92QAiDgaNS4wIBEIA788CgAuckPyPjzCvcOOyX9BAomy5Qh2r3rHYU1PJiWGlD3ZlDNHULIQdqf1hIJkkyaAQTzPuTvfImzOoCAAAAAAwAABAAAAAAAAAAAAAAAAAAAqu5qgKVOA2zH8XW0v78sP////8AAAABAAAAAAAAAAAAAAABAAAAGRaD/2a29GgJ/Y3GoDvEnFPf738IbVkA3r0/LzvF7PmPKbbL/4MV6jE1AGcwZQIxAKXBtAChs5Wxks9Hfu5usGselhRDOLbhVBtC9IQnVP5i3rDiOJzJefc/15vo/pel2wIwFSUoDWGY2sAEOEQRPiqW4pdyiQ2koB3divhP8+CWKpoOswhPRD25qilLIFyDKgm3";

        // Decrypt the data
        final CryptoResult<byte[], KmsMasterKey> decryptResult = crypto.decryptData(prov, Base64.getDecoder().decode(myresult));
//        final CryptoResult<String, KmsMasterKey> decryptResult = crypto.decryptString(prov, myresult);
        // Check the encryption context (and ideally the master key) to
        // ensure this is the expected ciphertext
        if (!decryptResult.getMasterKeyIds().get(0).equals(keyArn)) {
            throw new IllegalStateException("Wrong key id!");
        }

        // The SDK may add information to the encryption context, so check to
        // ensure all of the values are present
        for (final Map.Entry<String, String> e : context.entrySet()) {
            if (!e.getValue().equals(decryptResult.getEncryptionContext().get(e.getKey()))) {
                throw new IllegalStateException("Wrong Encryption Context!");
            }
        }
        // The data is correct, so output it.
        System.out.println("Decrypted: " + new String(decryptResult.getResult()));
    }


}
