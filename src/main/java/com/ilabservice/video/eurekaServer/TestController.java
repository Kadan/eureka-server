package com.ilabservice.video.eurekaServer;

import com.amazonaws.encryptionsdk.AwsCrypto;
import com.amazonaws.encryptionsdk.CryptoResult;
import com.amazonaws.encryptionsdk.kms.KmsMasterKey;
import com.amazonaws.encryptionsdk.kms.KmsMasterKeyProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.*;

@RestController
public class TestController {

    private static final byte[] EXAMPLE_DATA = "Hello World".getBytes(StandardCharsets.UTF_8);
    final  String keyArn = "arn:aws:kms:ap-southeast-1:460811381319:key/edd8576d-f2a9-477e-9d2d-b5a31233270e";

    @GetMapping("/say")
    public String second(HttpServletRequest request){
        String name = request.getParameter("name");
        return "here we go ... " + name;
    }


    @GetMapping("/kms/encrypt")
    public String kmsEncrypt(HttpServletRequest request){
        String data = request.getParameter("data");
        // Instantiate the SDK
        final AwsCrypto crypto = new AwsCrypto();
        // Set up the master key provider
        List keyList = new ArrayList<>();
        keyList.add(keyArn);
        final KmsMasterKeyProvider prov = KmsMasterKeyProvider.builder().withKeysForEncryption(keyList).build();//new KmsMasterKeyProvider(keyArn);
        final Map<String, String> context = Collections.singletonMap("Example", "String");
        final CryptoResult<byte[], KmsMasterKey> encryptResult = crypto.encryptData(prov, data.getBytes(), context);
        final byte[] ciphertext = encryptResult.getResult();
        String encoded = Base64.getEncoder().encodeToString(ciphertext);

        System.out.println("Ciphertext: " + encoded);

        //new TestController().decrypt(encoded);

        return "hello world " + encoded;
    }

    @GetMapping("/decrypt")
    public String decrypt(String ciphertext){
//        String ciphertext = request.getParameter("ctext");
        // Instantiate the SDK
        final AwsCrypto crypto = new AwsCrypto();

        // Set up the master key provider
        List keyList = new ArrayList<>();
        keyList.add(keyArn);
        final KmsMasterKeyProvider prov =  KmsMasterKeyProvider.builder().withKeysForEncryption(keyList).build();
        final Map<String, String> context = Collections.singletonMap("Example", "String");
        // Decrypt the data
        final CryptoResult<byte[], KmsMasterKey> decryptResult = crypto.decryptData(prov, Base64.getDecoder().decode(ciphertext));
        //final CryptoResult<String, KmsMasterKey> decryptResult = crypto.decryptString(prov, ciphertext);
        // Check the encryption context (and ideally the master key) to
        // ensure this is the expected ciphertext
        if (!decryptResult.getMasterKeyIds().get(0).equals(keyArn)) {
            throw new IllegalStateException("Wrong key id!");
        }

        // The SDK may add information to the encryption context, so check to
        // ensure all of the values are present
        for (final Map.Entry<String, String> e : context.entrySet()) {
            if (!e.getValue().equals(decryptResult.getEncryptionContext().get(e.getKey()))) {
                throw new IllegalStateException("Wrong Encryption Context!");
            }
        }
        // The data is correct, so output it.
        System.out.println("Decrypted: " + new String(decryptResult.getResult()));

        return "hello world " + new String(decryptResult.getResult());
    }


    @GetMapping("/kms/decrypt")
    public String kmsDecrypt(HttpServletRequest request){
        String ciphertext = request.getParameter("ctext").replaceAll(" ","+").replaceAll("\r\n","");
//        String ciphertext = StringUtils.replaceAll(request.getParameter("ctext")," ","+");
        // Instantiate the SDK
        final AwsCrypto crypto = new AwsCrypto();

        // Set up the master key provider
        List keyList = new ArrayList<>();
        keyList.add(keyArn);
        final KmsMasterKeyProvider prov =  KmsMasterKeyProvider.builder().withKeysForEncryption(keyList).build();
        final Map<String, String> context = Collections.singletonMap("Example", "String");
        // Decrypt the data
        final CryptoResult<byte[], KmsMasterKey> decryptResult = crypto.decryptData(prov, Base64.getDecoder().decode(ciphertext));
        //final CryptoResult<String, KmsMasterKey> decryptResult = crypto.decryptString(prov, ciphertext);
        // Check the encryption context (and ideally the master key) to
        // ensure this is the expected ciphertext
        if (!decryptResult.getMasterKeyIds().get(0).equals(keyArn)) {
            throw new IllegalStateException("Wrong key id!");
        }

        // The SDK may add information to the encryption context, so check to
        // ensure all of the values are present
        for (final Map.Entry<String, String> e : context.entrySet()) {
            if (!e.getValue().equals(decryptResult.getEncryptionContext().get(e.getKey()))) {
                throw new IllegalStateException("Wrong Encryption Context!");
            }
        }
        // The data is correct, so output it.
        System.out.println("Decrypted: " + new String(decryptResult.getResult()));

        return "hello world " + new String(decryptResult.getResult());
    }


    @PostMapping(value="post")
    public String postTest(HttpServletRequest request,@RequestHeader Map<String, String> headers){
        String post = request.getParameter("post");
        StringBuilder content = new StringBuilder();
        headers.forEach((key, value) -> content.append(key).append("=").append(value).append(";"));




        return "hello this is your parameter post = " + post + " and this is your header " + content.toString();
    }


    @GetMapping("/foo/bar")
    public String first(){
        return "foo.bar";
    }
    @GetMapping("/hello")
    public String second(){
        return "hello.world";
    }
    @GetMapping("/test")
    public String custIp(HttpServletRequest request){
        String ip = request.getHeader("x-forwarded-for");
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = request.getHeader("Proxy-Client-IP");

        }

        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = request.getHeader("WL-Proxy-Client-IP");

        }

        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {

            ip = request.getRemoteAddr();

        }

        if (ip.contains(",")) {
            return ip.split(",")[0];
        } else {
            return ip;
        }
    }

}
